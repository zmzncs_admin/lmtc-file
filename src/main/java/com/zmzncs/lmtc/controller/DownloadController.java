package com.zmzncs.lmtc.controller;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.OSSObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import java.io.*;

@Api(tags = "文件下载")
@Controller("DownloadController")
@RequestMapping("/download")
@Scope("prototype")
@Validated
public class DownloadController {

    @ApiOperation(value = "下载文件")
    @PostMapping(value = "/download-bykey")
    public void downloadFile() throws IOException {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletResponse response = servletRequestAttributes.getResponse();

        File file = new File("E://123.jpg");
        InputStream inputStream = new FileInputStream(file);
        OutputStream outputStream = response.getOutputStream();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);

        try  {
            response.setContentType("application/octet-stream; charset=UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=" + new String("55".getBytes("UTF-8"), "ISO-8859-1"));
            int bytesRead;
            byte[] buffer = new byte[1024];
            while((bytesRead = inputStream.read(buffer, 0, 1024)) != -1) {
                bufferedOutputStream.write(buffer, 0, bytesRead);
            }
            bufferedOutputStream.flush();
        } catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                inputStream.close();
                outputStream.close();
                bufferedOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @ApiOperation(value = "下载阿里oss的文件")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "fileKey", value = "fileKey", required = true),
            @ApiImplicitParam(paramType = "query", name = "name", value = "name"),
    })
    @PostMapping(value = "/download-oss")
    public void downloadFile(@NotBlank String fileKey, @NotBlank String name) throws IOException {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletResponse response = servletRequestAttributes.getResponse();

        String endpoint = "http://oss-cn-beijing.aliyuncs.com";
        String accessKeyId = "LTAI7RDweCFYN01O";
        String accessKeySecret = "xG0haFDFCuVzPwH9cbO51EwrEWaH82";
        String bucketName = "guanyua";
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);

        OSSObject ossObject = ossClient.getObject(bucketName, fileKey);

        InputStream inputStream = ossObject.getObjectContent();
        OutputStream outputStream = response.getOutputStream();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);

        try  {
            response.setContentType("application/octet-stream; charset=UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=" + new String(name.getBytes("UTF-8"), "ISO-8859-1"));
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while((bytesRead = inputStream.read(buffer, 0, 1024)) != -1) {
                bufferedOutputStream.write(buffer, 0, bytesRead);
            }
            bufferedOutputStream.flush();
        } catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                inputStream.close();
                outputStream.close();
                bufferedOutputStream.close();
                ossClient.shutdown();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}