package com.zmzncs.lmtc;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.annotations.ApiIgnore;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * SwaggerConfig
 */
@Configuration
@EnableSwagger2
@EnableKnife4j
@Import(BeanValidatorPluginsConfiguration.class)
@ApiIgnore
public class Swagger2Config {

    @Bean
    public Docket ticket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(productApiInfo())
                .groupName("文件管理")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.zmzncs.lmtc.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo productApiInfo() {
        ApiInfo apiInfo = new ApiInfoBuilder()
                .title("立码停车-文件管理")
                .description("文档描述。。。")
                .version("1.0")
                .termsOfServiceUrl("API TERMS URL")
                .license("license")
                .licenseUrl("license url")
                .build();

        return apiInfo;
    }

}