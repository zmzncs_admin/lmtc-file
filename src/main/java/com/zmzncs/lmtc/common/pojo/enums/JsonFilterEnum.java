package com.zmzncs.lmtc.common.pojo.enums;

public enum JsonFilterEnum {

    SERIALIZEALLEXCEPT("serializeAllExcept", "serializeAllExcept"),
    FILTEROUTALLEXCEPT("filterOutAllExcept", "filterOutAllExcept");

    private String name;
    private String value;

    private JsonFilterEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return this.name;
    }

    public String getValue() {
        return this.value;
    }

    public String toString() {
        return this.value + "_" + this.name;
    }
}