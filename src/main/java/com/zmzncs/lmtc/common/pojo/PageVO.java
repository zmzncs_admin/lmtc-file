package com.zmzncs.lmtc.common.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;


@Data
public class PageVO<T> {

    @ApiModelProperty(value = "页码")
    private Integer current;

    @ApiModelProperty(value = "总页数")
    private Integer pages;

    @ApiModelProperty(value = "每页记录数")
    private Integer size;

    @ApiModelProperty(value = "总记录数")
    private Integer total;

    @ApiModelProperty(value = "当前页记录列表")
    private List<T> records;

}
