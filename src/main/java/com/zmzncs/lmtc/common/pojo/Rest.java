package com.zmzncs.lmtc.common.pojo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class Rest<T> implements Serializable{

	private static final long serialVersionUID = 5952861126789622536L;

    @ApiModelProperty(value="状态码：【｛200：操作成功｝，｛400：参数错误｝，｛401：用户认证失败｝，｛403：服务器不处理｝，｛500：服务器错误｝】")
	private Integer code;
    @ApiModelProperty(value="描述")
	private String desc;
    @ApiModelProperty(value="数据")
	private T data;

    public Rest() {
		super();
	}

    public Integer getCode() {
        return code;
    }

    public Rest<T> setCode(Integer code) {
        this.code = code;
        return this;
    }

    public String getDesc() {
        desc = desc == null ? "无回复说明！" : desc;
        return desc;
    }

    public Rest<T> setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public T getData() {
        return data;
    }

    public Rest<T> setData(T data) {
        this.data = data;
        return this;
    }
}

	
