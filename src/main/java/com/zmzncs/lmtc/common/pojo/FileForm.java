package com.zmzncs.lmtc.common.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 手牌
 * </p>
 *
 * @author 任建波
 * @since 2019-09-09
 * ignoreUnknown设置为true说明在反序列化的时候忽视未知的字段，即反序列的时候能对应的就对应上，不能对应的就不管了。
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FileForm implements Serializable {

    private static final long serialVersionUID = 1L;

//    @ApiModelProperty(value="文件类型，值必须为img或audio或file或video", required = true)
//    @FileTypeValidator(values = "img, audio, file, video")
//    private String type;

    @ApiModelProperty(value="文件", required = true)
    @NotNull
    private MultipartFile[] files;


    public MultipartFile[] getFiles() {
        return files;
    }

    public void setFiles(MultipartFile[] files) {
        this.files = files;
    }
}
