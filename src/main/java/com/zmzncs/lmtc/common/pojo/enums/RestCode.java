package com.zmzncs.lmtc.common.pojo.enums;

public enum RestCode {

    CODE_200(200, "成功"),
    CODE_201(201, "已在服务器上成功创建了一个或多个新资源"),
    CODE_400(400, "参数错误"),
    CODE_401(401, "用户认证失败"),
    CODE_403(403, "服务器不处理"),
    CODE_500(500, "服务器错误"),
    CODE_502(502, "不正确或不完全编程");

    private int code;
    private String desc;

    RestCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}