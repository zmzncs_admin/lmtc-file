package com.zmzncs.lmtc.common.advice;

import com.google.common.collect.Lists;
import com.zmzncs.lmtc.common.exception.BusinessException;
import com.zmzncs.lmtc.common.exception.ParamException;
import com.zmzncs.lmtc.common.pojo.Rest;
import com.zmzncs.lmtc.common.util.RestUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolationException;
import java.util.List;


/**
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 业务异常
     */
    @ResponseBody
    @ExceptionHandler(BusinessException.class)
    @ResponseStatus(HttpStatus.OK)
    public Rest businessException(BusinessException exception) {
        logger.error(exception.getMessage());
        return RestUtil.warning(403, exception.getMessage());
    }

    /**
     * 参数校验异常
     */
    @ResponseBody
    @ExceptionHandler(ParamException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Rest businessException(ParamException exception) {
        logger.error(exception.getMessage());
        return RestUtil.warning(400, exception.getMessage());
    }

    /**
     * 请求类型错误ContentType
     */
    @ResponseBody
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Rest httpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException exception) {
        logger.error("ContentType不支持" + exception.getContentType() + "，请使用" + exception.getSupportedMediaTypes());
        return RestUtil.warning(400, "ContentType不支持" + exception.getContentType() + "，请使用" + exception.getSupportedMediaTypes());
    }

    /**
     * 请求方式post，get等
     */
    @ResponseBody
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Rest HttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException exception) {
        logger.error("请求方式不支持" + exception.getMethod() + "，请使用" + exception.getSupportedHttpMethods());
        return RestUtil.warning(400, "请求方式不支持" + exception.getMethod() + "，请使用" + exception.getSupportedHttpMethods());
    }

    /**
     * BindException参数校验未通过异常
     */
    @ResponseBody
    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Rest bindException(BindException exception) {
        FieldError fieldError = exception.getBindingResult().getFieldError();
        assert fieldError != null;
        logger.warn(fieldError.getField() + ":" + fieldError.getDefaultMessage());
        return RestUtil.warning(400, "参数错误" , fieldError.getField() + ":" + fieldError.getDefaultMessage());
    }

    /**
     * 参数校验未通过异常 @RequestBody参数校验失败
     */
    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Rest methodArgumentNotValidException(MethodArgumentNotValidException exception) {
        List<ObjectError> errors = exception.getBindingResult().getAllErrors();
        StringBuffer sb = new StringBuffer();
        List<String> errorArr = Lists.newArrayList();
        for (ObjectError error : errors) {
            if (error instanceof FieldError){
                FieldError fieldError=(FieldError)error;
                errorArr.add(fieldError.getField()+fieldError.getDefaultMessage());
            }else{
                errorArr.add(error.getObjectName()+error.getDefaultMessage());
            }
        }
        logger.error("参数错误：" + errorArr.toString());
        return RestUtil.warning(400, "参数错误", errorArr.toArray(new String[]{}));
    }

    /**
     * @RequestParam 参数校验失败
     */
    @ResponseBody
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Rest constraintViolationException(ConstraintViolationException exception) {
        logger.error("参数错误：" + exception.getMessage());

        return RestUtil.warning(400, "参数错误", exception.getMessage());
    }

//    @ResponseBody
//    @ExceptionHandler(Exception.class)
//    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
//    public Rest globalException(Exception exception){
//        exception.printStackTrace();
//        logger.error(exception.getMessage());
//        return RestUtil.error("服务器错误", exception.getMessage());
//    }

}