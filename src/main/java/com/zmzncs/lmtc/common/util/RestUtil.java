package com.zmzncs.lmtc.common.util;

import com.zmzncs.lmtc.common.pojo.Rest;

public class RestUtil {

    public static Rest success(){
        Rest rest = new Rest();
        rest.setCode(200);
        rest.setData(null);
        rest.setDesc("success");
        return rest;
    }

    public static Rest success(int code, Object object){
        Rest rest = new Rest();
        rest.setCode(code);
        rest.setData(object);
        rest.setDesc("success");
        return rest;
    }

    public static Rest success(Object object){
        Rest rest = new Rest();
        rest.setCode(200);
        rest.setData(object);
        rest.setDesc("success");
        return rest;
    }

    public static Rest warning(String desc){
        Rest rest = new Rest();
        rest.setCode(403);
        rest.setData(null);
        rest.setDesc(desc);
        return rest;
    }

    public static Rest warning(Integer code, String desc){
        Rest rest = new Rest();
        rest.setCode(code);
        rest.setData(null);
        rest.setDesc(desc);
        return rest;
    }

    public static Rest warning(Integer code, String desc, Object object){
        Rest rest = new Rest();
        rest.setCode(code);
        rest.setData(object);
        rest.setDesc(desc);
        return rest;
    }

    public static Rest error(){
        Rest rest = new Rest();
        rest.setCode(500);
        rest.setData(null);
        rest.setDesc("error");
        return rest;
    }

    public static Rest error(int code, String desc){
        Rest rest = new Rest();
        rest.setCode(code);
        rest.setData(null);
        rest.setDesc(desc);
        return rest;
    }

    public static Rest error(String desc){
        Rest rest = new Rest();
        rest.setCode(500);
        rest.setData(null);
        rest.setDesc(desc);
        return rest;
    }

    public static Rest error(String desc, Object object){
        Rest rest = new Rest();
        rest.setCode(500);
        rest.setData(object);
        rest.setDesc(desc);
        return rest;
    }

}

	
