package com.zmzncs.lmtc.common.util;

import com.zmzncs.lmtc.common.exception.BusinessException;
import com.zmzncs.lmtc.common.exception.ParamException;

public class ExceptionUtil {

    /**
     *  业务异常
     */
    public static void businessException(String message) {
        throw new BusinessException(message);
    }

    /**
     *  参数校验异常
     */
    public static void paramException(String message) {
        throw new ParamException(message);
    }

}
