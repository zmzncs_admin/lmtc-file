package com.zmzncs.lmtc.common.util;

import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;


import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;


/**
 * 图片压缩上传
 *
 * @Author 高博文
 * @Date 2020/11/03 17:01
 */
@Slf4j
@Component
public class CompressImage {


    /**
     * 压缩图片并上传
     * @param file 原文件
     * @param filePath 原文件全目录
     */
//    @Async
    public void generateFixedSizeImage(MultipartFile file,String filePath) {
        String imageFormat = filePath.substring(filePath.lastIndexOf("."));
        //压缩后图片名称
        String ImageName = "@_@350" + imageFormat;
        //去除文件后缀名
        String path = filePath.substring(0,filePath.lastIndexOf("."));

        //压缩后图片全路径
        String newPath = path + ImageName;
        try {
            Thumbnails.of(file.getInputStream())    //图片原文件
                    .size(350,175)  //设置压缩后的图片大小
                    .toFile(newPath);   //生成新图片地址
            log.info("图片压缩成功,图片路径==>{}",newPath);
        } catch (IOException e) {
            log.info("图片压缩失败!");
            e.printStackTrace();
        }
    }




//    /**
//     * 使用给定的图片生成指定大小的图片
//     */
//    @Async
//    public void generateFixedSizeImage(){
//        try {
//            Thumbnails.of("data/meinv.jpg").size(350,175).toFile("data/newmeinv.jpg");
//        } catch (IOException e) {
//            System.out.println("原因: " + e.getMessage());
//        }
//    }
//
//    /**
//     * 转换图片格式,将流写入到输出流
//     */
//    @Async
//    public void generateOutputstream(){
//
//        try(
//                OutputStream outputStream = new FileOutputStream("data/2016010208_outputstream.png")) { //自动关闭流
//             Thumbnails.of("data/2016010208.jpg").
//                    size(350,175).
//                    outputFormat("png"). // 转换格式
//                    toOutputStream(outputStream); // 写入输出流
//        } catch (IOException e) {
//            System.out.println("原因: " + e.getMessage());
//        }
//    }
//
//    /**
//     * 按比例缩放图片
//     * @param imagePath 要压缩图片名称
//     * @param newImagePath 压缩后图片名称
//     * @param scale 压缩比例
//     */
//    @Async
//    public void generateScale(String imagePath,String newImagePath,Double scale){
//        try {
//            Thumbnails.of(imagePath).
//                    //scalingMode(ScalingMode.BICUBIC).
//                            scale(scale). // 图片缩放80%, 不能和size()一起使用
//                    outputQuality(scale). // 图片质量压缩80%
//                    toFile(newImagePath);
//        } catch (IOException e) {
//            System.out.println("原因: " + e.getMessage());
//        }
//    }
//
//    /**
//     * 生成缩略图到指定的目录
//     * @param imagePath 要压缩图片名称
//     * @param newImagePath 压缩后图片名称
//     * @param filePath 压缩后文件保存文件夹
//     * @param scale 压缩比例
//     */
//    @Async
//    public void generateThumbnail2Directory(String imagePath,String newImagePath,String filePath,Double scale){
//        try {
//            Thumbnails.of(imagePath,newImagePath).
//                    //scalingMode(ScalingMode.BICUBIC).
//                            scale(scale). // 图片缩放80%, 不能和size()一起使用
//                    toFiles(new File(filePath), Rename.NO_CHANGE);//指定的目录一定要存在,否则报错
//        } catch (IOException e) {
//            System.out.println("原因: " + e.getMessage());
//        }
//    }
//
//    /**
//     * 将指定目录下所有图片生成缩略图
//     * @param filePath 需要压缩的文件夹
//     * @param NewFilePath 压缩后的文件存储位置
//     * @param scale 压缩比例
//     */
//    @Async
//    public void generateDirectoryThumbnail(String filePath,String NewFilePath,Double scale){
//        try {
//            Thumbnails.of(new File(filePath).listFiles()).
//                    //scalingMode(ScalingMode.BICUBIC).
//                            scale(scale). // 图片缩放80%, 不能和size()一起使用
//                    toFiles(new File(NewFilePath), Rename.SUFFIX_HYPHEN_THUMBNAIL);//指定的目录一定要存在,否则报错
//        } catch (IOException e) {
//            System.out.println("原因: " + e.getMessage());
//        }
//    }
}
