package com.zmzncs.lmtc.common.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.zmzncs.lmtc.common.pojo.PageVO;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * json转换类
 */
public class JsonUtil {

    public static final String serializeAllExcept = "serializeAllExcept";
    public static final String filterOutAllExcept = "filterOutAllExcept";

    /**
     * 对象转json字符串
     */
    public static <T> String objectToJson(T T){
        String json="";
        try {
            ObjectMapper jsonMapper = new ObjectMapper();
            jsonMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
            json = jsonMapper.writeValueAsString(T);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * 将json字符串转化为实体POJO
     */
    public static <T> T JSONToObj(String jsonStr, Class<T> T) {
        T t = null;
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            t = objectMapper.readValue(jsonStr, T);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }

    /**
     * 返回json 节点
     */
    public static JsonNode getJsonNode(String jsonStr) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        JsonNode jsonNode = null;
        try {
            jsonNode = objectMapper.readTree(jsonStr);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return jsonNode;
    }

    /**
     * 返回json的value
     */
    public static String getJsonValue(JsonNode node, String key) {
        JsonNode result = node.get(key);
        if(result!=null){
            if(result.getNodeType().name().equalsIgnoreCase("STRING")){
                return result.asText();
            }else {
                return result.toString();
            }
        }
        return null;
    }


    /**
     * 过滤字段
     * @param filterName 需要跟类上的注解@JsonFilter("xxx")里面的一致
     * @param beanProperties 过滤的字段
     * @param type  serializeAllExcept 表示序列化全部，除了指定字段。 filterOutAllExcept 表示过滤掉全部，除了指定的字段。
     *
     */
    public static ObjectMapper setupJsonFilter(String filterName, String[] beanProperties, String type){
        if(StringUtils.isBlank(filterName) || StringUtils.isAnyBlank(beanProperties) ){
            ObjectMapper mapper = new ObjectMapper();
            try {
                mapper.writeValueAsString("缺少参数");
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            return mapper;
        }

        ObjectMapper mapper = new ObjectMapper();
        //serializeAllExcept 表示序列化全部，除了指定字段。
        if(type.equals(JsonUtil.serializeAllExcept)){
            FilterProvider filterProvider = new SimpleFilterProvider()
                    .addFilter(filterName, SimpleBeanPropertyFilter.serializeAllExcept(beanProperties));

            mapper.setFilterProvider(filterProvider);
            return mapper;
        }
        // filterOutAllExcept 表示过滤掉全部，除了指定的字段。
        if(type.equals(JsonUtil.filterOutAllExcept)){
            FilterProvider filterProvider = new SimpleFilterProvider()
                    .addFilter(filterName, SimpleBeanPropertyFilter.filterOutAllExcept(beanProperties));

            mapper.setFilterProvider(filterProvider);
            return mapper;
        }
        //  type不对
        try {
            mapper.writeValueAsString("type只能是serializeAllExcept或者filterOutAllExcept");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return mapper;
    }

    /**
     * list泛型转型
     */
    public static <T> List<T> listTransform(List oldList, Class<T> beanType){
        List<T> list = new ArrayList<>();
        for(Object object: oldList){
            //  Feature.OrderedField：保持顺序
            T t = JSONObject.parseObject(JSON.toJSONString(object), beanType, Feature.OrderedField);
            list.add(t);
        }

        return list;
    }


    public static void  main(String[] args){
        String s = "{\"addresslist-explorer\":{\"result\":{},\"query\":{\"display-name\":\"3333\"}}}";
        JsonNode node = JsonUtil.getJsonNode(s);
        JsonNode node1 = JsonUtil.getJsonNode(JsonUtil.getJsonValue(node,"addresslist-explorer"));
        System.out.println(JsonUtil.getJsonValue(node,"addresslist-explorer"));
        System.out.println(JsonUtil.getJsonValue(node1,"result"));
    }

}
