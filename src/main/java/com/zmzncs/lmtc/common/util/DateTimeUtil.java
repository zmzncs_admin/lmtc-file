package com.zmzncs.lmtc.common.util;


//import org.apache.commons.lang.StringUtils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;

/**
* Created by linjie on 2018/8/01.
*/
@Slf4j
public class DateTimeUtil {

    private final static String DATEFORMAT_YYYYMMDD = "yyyyMMdd";
    private final static String DATEFORMAT_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    private final static String DATEFORMAT_YYYYMMDDHHMMSSSSS = "yyyyMMddHHmmssSSS";
    private final static SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
    private final static SimpleDateFormat sdfDay = new SimpleDateFormat("yyyy-MM-dd");
    private final static SimpleDateFormat sdfDays = new SimpleDateFormat("yyyyMMdd");
    private final static SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private final static SimpleDateFormat sdfTimes = new SimpleDateFormat("yyyyMMddHHmmss");

    public static void main(String[] args) {
        log.info("{}", stampToTime(1601395200));
//        log.info("{}", stampToTime(1601476404));
    }

    /**
     * 获取yyyyMMddHHmmss格式
     */
    public static String getSdfTimes() {
        return sdfTimes.format(new Date());
    }

    /**
     * 获取yyyy格式
     * @return
     */
    public static String getYear() {
        return sdfYear.format(new Date());
    }

    /**
     * 获取yyyy-MM-dd格式
     */
    public static String getDay() {
        return sdfDay.format(new Date());
    }

    /**
     * 获取yyyyMMdd格式
     */
    public static String getDays(){
        return sdfDays.format(new Date());
    }

    /**
     * 获取yyyy-MM-dd HH:mm:ss格式
     */
    public static String getTime() {
        return sdfTime.format(new Date());
    }

    /**
     * 获取YYYY-MM-DD格式
     * @return
     */
    public static String convertYMD(Date date) {
        return sdfDay.format(date);
    }

    /**
     * 获取YYYY-MM-DD HH:mm:ss格式
     * @return
     */
    public static String convertYMDHMS(Date date) {
        return sdfTime.format(date);
    }


    /**
     * 格式化日期yyyy-MM-dd
     */
    public static Date fomatDate(String date) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return fmt.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 格式化日期
     */
    public static Date fomatDateTime(String date) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return fmt.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 两个时间之间相差距离多少天
     */
    public static long getDistanceDays(Date startTime, Date endTime) {
        long days;
        long time1 = startTime.getTime();
        long time2 = endTime.getTime();
        long diff ;
        if(time1<time2) {
            diff = time2 - time1;
        } else {
            diff = time1 - time2;
        }
        days = diff / (1000 * 60 * 60 * 24);
        return days;//返回相差多少天
    }

    /**
     * 时间差，天、小时、分、秒
     */
    public static long[] getDistanceTimes(String starttime, String endtime) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date one;
        Date two;
        long day = 0;
        long hour = 0;
        long min = 0;
        long sec = 0;
        try {
            one = df.parse(starttime);
            two = df.parse(endtime);
            long time1 = one.getTime();
            long time2 = two.getTime();
            long diff ;
            if(time1<time2) {
                diff = time2 - time1;
            } else {
                diff = time1 - time2;
            }
            day = diff / (24 * 60 * 60 * 1000);
            hour = (diff / (60 * 60 * 1000) - day * 24);
            min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
            sec = (diff/1000-day*24*60*60-hour*60*60-min*60);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long[] times = {day, hour, min, sec};
        return times;
    }

    /**
     * 两个时间相差距离多少天多少小时多少分多少秒
     * @return String 返回值为：xx天xx小时xx分xx秒
     */
    public static String getDistanceTime(String starttime, String endtime) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date one;
        Date two;
        long day = 0;
        long hour = 0;
        long min = 0;
        long sec = 0;
        try {
            one = df.parse(starttime);
            two = df.parse(endtime);
            long time1 = one.getTime();
            long time2 = two.getTime();
            long diff ;
            if(time1<time2) {
                diff = time2 - time1;
            } else {
                diff = time1 - time2;
            }
            day = diff / (24 * 60 * 60 * 1000);
            hour = (diff / (60 * 60 * 1000) - day * 24);
            min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
            sec = (diff/1000-day*24*60*60-hour*60*60-min*60);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return day + "天" + hour + "小时" + min + "分" + sec + "秒";
    }

    /**
     *  时间戳转日期格式
     */
    public static String secToTime(int time) {
        String timeStr = null;
        int hour = 0;
        int minute = 0;
        int second = 0;
        if (time <= 0)
            return "00:00";
        else {
            minute = time / 60;
            if (minute < 60) {
                second = time % 60;
                timeStr = unitFormat(minute) + ":" + unitFormat(second);
            } else {
                hour = minute / 60;
                if (hour > 99)
                    return "99:59:59";
                minute = minute % 60;
                second = time - hour * 3600 - minute * 60;
                timeStr = unitFormat(hour) + ":" + unitFormat(minute) + ":" + unitFormat(second);
            }
        }
        return timeStr;
    }

    public static String unitFormat(int i) {
        String retStr = null;
        if (i >= 0 && i < 10)
            retStr = "0" + Integer.toString(i);
        else
            retStr = "" + i;
        return retStr;
    }

    /**
     * 计算时间差 小时 分钟
     */
    public static String getDateDifferenceHourMin(Date endDate, Date beginDate) {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - beginDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        long sec = diff % nd % nh % nm / ns;
        if(day>=0){
            day = day*24;
        }
        hour = day+hour;

        String m = String.valueOf(min);
        String h = String.valueOf(hour);

        if(min<10){
            m = "0"+min;
        }

        if(hour<10){
            h = "0"+hour;
        }

        return  h + ":" + m;
    }

    /**
     * 计算时间差 小时 分钟 秒
     */
    public static String getDatePoor(Date endDate, Date nowDate) {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        long sec = diff % nd % nh % nm / ns;
        if(day>=0){
            day = day*24;
        }
        hour = day+hour;

        String s = String.valueOf(sec);
        String m = String.valueOf(min);
        String h = String.valueOf(hour);

        if(sec<10){
            s = "0"+sec;
        }

        if(min<10){
            m = "0"+min;
        }

        if(hour<10){
            h = "0"+hour;
        }

        return  h + ":" + m + ":"+s;
    }

    /**
     * 获取时间周区间
     */
   public static HashMap<String, Object> getDate(String time) throws ParseException {

       HashMap<String, Object> m = new HashMap<String, Object>();

       SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
       SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

       Date date = null;
       date = format.parse(time);

       Calendar calendar = Calendar.getInstance();
       calendar.setFirstDayOfWeek(Calendar.MONDAY);
       calendar.setTime(date);

       Integer year = Integer.parseInt(yearFormat.format(date));
       Integer week = calendar.get(Calendar.WEEK_OF_YEAR);
       Integer targetNum = 0;
       if (week + targetNum > 52) {
           year++;
           week += targetNum - 52;
       } else if (week + targetNum <= 0) {
           year--;
           week += targetNum + 52;
       } else {
           week += targetNum;
       }
       Calendar cal = Calendar.getInstance();
       // 设置每周的开始日期
       cal.setFirstDayOfWeek(Calendar.SUNDAY);

       cal.set(Calendar.YEAR, year);
       cal.set(Calendar.WEEK_OF_YEAR, week);

       cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
       String beginDate = format.format(cal.getTime());

       cal.add(Calendar.DAY_OF_WEEK, 6);
       String endDate = format.format(cal.getTime());
       m.put("beginDate", beginDate);
       m.put("endDate", endDate);
       return m;
   }

   public static String getWeekOfDate(Date date) {
       String[] weekDaysName = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
       //String[] weekDaysCode = { "0", "1", "2", "3", "4", "5", "6" };
       Calendar calendar = Calendar.getInstance();
       calendar.setTime(date);
       int intWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
       return weekDaysName[intWeek];
   }

   public static List<String> getDateList(String date1,String date2)throws ParseException{
       SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
       List<String> list=new ArrayList<>();
           Date d1 = sdf.parse(date1);
           Date d2 = sdf.parse(date2);
           Calendar c = Calendar.getInstance();
           c.setTime(d1);


           System.out.println(sdf.format(c.getTime()));//打出第一天的
           list.add(sdf.format(c.getTime()));
           do{
               c.add(Calendar.DATE, 1);//日期加1
              // System.out.println(sdf.format(c.getTime()));
               list.add(sdf.format(c.getTime()));
           }
           while(!c.getTime().equals(d2));//直到和第二个日期相等，跳出循环
           return list;
   }

   
     public static int getGapCount(Date startDate, Date endDate) {
       Calendar fromCalendar = Calendar.getInstance(); 
       fromCalendar.setTime(startDate); 
       fromCalendar.set(Calendar.HOUR_OF_DAY, 0); 
       fromCalendar.set(Calendar.MINUTE, 0); 
       fromCalendar.set(Calendar.SECOND, 0); 
       fromCalendar.set(Calendar.MILLISECOND, 0); 
    
       Calendar toCalendar = Calendar.getInstance(); 
       toCalendar.setTime(endDate); 
       toCalendar.set(Calendar.HOUR_OF_DAY, 0); 
       toCalendar.set(Calendar.MINUTE, 0); 
       toCalendar.set(Calendar.SECOND, 0); 
       toCalendar.set(Calendar.MILLISECOND, 0); 
    
       return (int) ((toCalendar.getTime().getTime() - fromCalendar.getTime().getTime()) / (1000 * 60 * 60 * 24));
   }

    /**
     * 获得某天最大时间 2017-10-15 23:59:59
     */
   public static Date getEndOfDay(Date date) {
       LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getTime()), ZoneId.systemDefault());;
       LocalDateTime endOfDay = localDateTime.with(LocalTime.MAX);
       return Date.from(endOfDay.atZone(ZoneId.systemDefault()).toInstant());
   }

    /**
     * 获得某天最小时间 2017-10-15 00:00:00
      */
   public static Date getStartOfDay(Date date) {
       LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getTime()), ZoneId.systemDefault());
       LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
       return Date.from(startOfDay.atZone(ZoneId.systemDefault()).toInstant());
   }

   /**
    * 判断当前时间是否在[startTime, endTime]区间，注意时间格式要一致
    * @param nowTime 当前时间
    * @param startTime 开始时间
    * @param endTime 结束时间
    */
   public static boolean isEffectiveDate(Date nowTime, Date startTime, Date endTime) {
       if (nowTime.getTime() == startTime.getTime()
               || nowTime.getTime() == endTime.getTime()) {
           return true;
       }


       Calendar date = Calendar.getInstance();
       date.setTime(nowTime);

       Calendar begin = Calendar.getInstance();
       begin.setTime(startTime);

       Calendar end = Calendar.getInstance();
       end.setTime(endTime);

       if (date.after(begin) && date.before(end)) {
           return true;
       } else {
           return false;
       }
   }


    //获取当前(上，下)周的日期范围如：...,-1上一周，0本周，1下一周...
    public static void getWeekDays(int i) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // getTimeInterval(sdf);

        Calendar calendar1 = Calendar.getInstance();
        // 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
        calendar1.setFirstDayOfWeek(Calendar.MONDAY);

        // 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了
        int dayOfWeek = calendar1.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天
        if (1 == dayOfWeek) {
            calendar1.add(Calendar.DAY_OF_MONTH, -1);
        }

        // 获得当前日期是一个星期的第几天
        int day = calendar1.get(Calendar.DAY_OF_WEEK);

        //获取当前日期前（下）x周同星几的日期
        calendar1.add(Calendar.DATE, 7*i);

        calendar1.add(Calendar.DATE, calendar1.getFirstDayOfWeek() - day);

        String beginDate = sdf.format(calendar1.getTime());
        calendar1.add(Calendar.DATE, 6);

        String endDate = sdf.format(calendar1.getTime());

        System.out.println(beginDate+" 到 "+endDate);
    }


    public static String getTimeInterval(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        // 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了
        int dayWeek = cal.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天
        if (1 == dayWeek) {
            cal.add(Calendar.DAY_OF_MONTH, -1);
        }
        // System.out.println("要计算日期为:" + sdf.format(cal.getTime())); // 输出要计算日期
        // 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        // 获得当前日期是一个星期的第几天
        int day = cal.get(Calendar.DAY_OF_WEEK);
        // 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
        cal.add(Calendar.DATE, cal.getFirstDayOfWeek() - day);
        String imptimeBegin = sdf.format(cal.getTime());
        // System.out.println("所在周星期一的日期：" + imptimeBegin);
        cal.add(Calendar.DATE, 6);
        String imptimeEnd = sdf.format(cal.getTime());
        // System.out.println("所在周星期日的日期：" + imptimeEnd);
        return imptimeBegin + "," + imptimeEnd;
    }

    /**
     * @Title: compareDate
     * @Description: TODO(日期比较，如果s>=e 返回true 否则返回false)
     */
    public static boolean compareDate(String s, String e) {
        if(fomatDate(s)==null||fomatDate(e)==null){
            return false;
        }
        return fomatDate(s).getTime() >=fomatDate(e).getTime();
    }

    /**
     * 校验日期是否合法
     */
    public static boolean isValidDate(String s) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        try {
            fmt.parse(s);
            return true;
        } catch (Exception e) {
            // 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            return false;
        }
    }

    /**
     * 获取相差几年
     */
    public static int getDiffYear(String startTime,String endTime) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        try {
            int years=(int) (((fmt.parse(endTime).getTime()-fmt.parse(startTime).getTime())/ (1000 * 60 * 60 * 24))/365);
            return years;
        } catch (Exception e) {
            // 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            return 0;
        }
    }

    /**
     * <li>功能描述：时间相减得到天数
     */
    public static long getDaySub(String beginDateStr, String endDateStr){
        long day=0;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date beginDate = null;
        Date endDate = null;

        try {
            beginDate = format.parse(beginDateStr);
            endDate= format.parse(endDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        day=(endDate.getTime()-beginDate.getTime())/(24*60*60*1000);
        //System.out.println("相隔的天数="+day);

        return day;
    }

    /**
     * 得到n天之后的日期
     */
    public static String getAfterDayDate(String days) {
        int daysInt = Integer.parseInt(days);

        Calendar canlendar = Calendar.getInstance(); // java.util包
        canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
        Date date = canlendar.getTime();

        SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = sdfd.format(date);

        return dateStr;
    }

    /**
     * 得到n天之后的日期
     */
    public static String getAfterDayDateHMS(String days) {
        int daysInt = Integer.parseInt(days);

        Calendar canlendar = Calendar.getInstance(); // java.util包
        canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
        Date date = canlendar.getTime();

        SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateStr = sdfd.format(date);

        return dateStr;
    }


    /**
     * 得到n天之后是周几
     */
    public static String getAfterDayWeek(String days) {
        int daysInt = Integer.parseInt(days);
        Calendar canlendar = Calendar.getInstance(); // java.util包
        canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
        Date date = canlendar.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("E");
        String dateStr = sdf.format(date);
        return dateStr;
    }

    /**
     * localDate转date
     */
    public static Date localDate2Date(LocalDate localDate) {
        if(null == localDate) {
            return null;
        }
        ZonedDateTime zonedDateTime = localDate.atStartOfDay(ZoneId.systemDefault());
        return Date.from(zonedDateTime.toInstant());
    }

    /**
     * date转localDate
     */
    public static LocalDate date2LocalDate(Date date) {
        if(null == date) {
            return null;
        }
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    /**
     * 计算两个时间相差天数
     */
    public static int betweenDays(Date endTime, Date beginTime){
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(endTime);
        long endTimeInMillis = calendar.getTimeInMillis();

        calendar.setTime(beginTime);
        long beginTimeInMillis = calendar.getTimeInMillis();

        int betweenDays = (int)((endTimeInMillis - beginTimeInMillis) / (1000L*3600L*24L));
        return betweenDays;
    }

    /**
     * 获取某周得第一天
     * @param year 年
     * @param week 周
     */
    public static Date getFirstDayOfWeek(int year, int week){
        Calendar c = new GregorianCalendar();

        c.set(Calendar.YEAR, year);
        c.set(Calendar.WEEK_OF_YEAR,  week);
        c.setFirstDayOfWeek(Calendar.MONDAY);
        c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek());
        c.set(Calendar.HOUR, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);

        return c.getTime();
    }

    /**
     * 获取某周得最后一天
     * @param year 年
     * @param week 周
     */
    public static Date getLastDayOfWeek(int year, int week){
        Calendar c = new GregorianCalendar();

        c.set(Calendar.YEAR, year);
        c.set(Calendar.WEEK_OF_YEAR,  week);
        c.setFirstDayOfWeek(Calendar.MONDAY);
        c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek() + 6);
        c.set(Calendar.HOUR, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);

        return c.getTime();
    }

    /**
     * 获取一天的开始时间
     */
    public static Date getDayStart(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date start = calendar.getTime();
        return start;
    }

    /**
     * 获取一天的结束时间
     */
    public static Date getDayEnd(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        Date end = calendar.getTime();

        return end;
    }

    /**
     * 获取num个月后的时间
     */
    public static Date getDateAfterbyMonth(Date date,Integer num){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, num);
        Date newDate = cal.getTime();
        return newDate;
    }

    /**
     * 字符串转时间格式
     */
    public static Date stringToDate(String date) throws Exception{
        if (date.length()>10){
            SimpleDateFormat sdf =   new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
            Date date1 = sdf.parse(date);
            return date1;
        }else {
            SimpleDateFormat sdf =   new SimpleDateFormat( "yyyy-MM-dd");
            Date date1 = sdf.parse(date);
            return date1;
        }
    }

    /**
     * 获取指定日期对应的前n分钟的日期.
     */
    public static Date getCurrUpMinuteDate(Date date, int n) {
        Calendar c = new GregorianCalendar();
        c.setTime(date);
        c.add(Calendar.MINUTE, -n);
        return c.getTime();
    }

    /**
     * 获取当前日期对应的前n天的日期.
     */
    public static Date getDaysAgoDate(int n){
        Calendar c = new GregorianCalendar();
        c.add(Calendar.DATE, -n);
        return c.getTime();
    }

    /**
     * 获取当前日期对应的后n天的日期.
     */
    public static Date getCurrNextDayDate(int n) {
        Calendar c = new GregorianCalendar();
        c.add(Calendar.DATE, n);
        return c.getTime();
    }

    /**
     * 获取指定日期对应的后n天的日期.
     */
    public static Date getNextDayDate(Date date, int n) {
        Calendar c = new GregorianCalendar();
        c.setTime(date);
        c.add(Calendar.DATE, n);
        return c.getTime();
    }

    /**
     *  n月后日期
     */
    public static Date dateAddMonth(Date dt , int m){
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.MONTH, m);
        return c.getTime();
    }

    /**
     *  n年后日期
     */
    public static Date dateAddYear(Date dt , int y){
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.YEAR, y);
        return c.getTime();
    }

    public static String getDateStr(Date date, String formatStr){
        String format = "yyyyMMdd";
        if(!StringUtils.isBlank(formatStr)){
            format = formatStr;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    /**
     * 毫秒转换天、时、分、秒
     */
    public static String formatTime(long ms) {
        int ss = 1000;
        int mi = ss * 60;
        int hh = mi * 60;
        int dd = hh * 24;

        long day = ms / dd;
        long hour = (ms - day * dd) / hh;
        long minute = (ms - day * dd - hour * hh) / mi;
        long second = (ms - day * dd - hour * hh - minute * mi) / ss;
        long milliSecond = ms - day * dd - hour * hh - minute * mi - second * ss;

        String strDay = day < 10 ? "0" + day : "" + day; //天
        String strHour = hour < 10 ? "0" + hour : "" + hour;//小时
        String strMinute = minute < 10 ? "0" + minute : "" + minute;//分钟
        String strSecond = second < 10 ? "0" + second : "" + second;//秒
        String strMilliSecond = milliSecond < 10 ? "0" + milliSecond : "" + milliSecond;//毫秒
//        strMilliSecond = milliSecond < 100 ? "0" + strMilliSecond : "" + strMilliSecond;

        return strDay + "天" + strHour + "时" + strMinute + "分钟 " + strSecond + "秒";
    }

    /**
     * 毫秒转换天、时、分
     */
    public static String durationTime(long ms) {
        int ss = 1000;
        int mi = ss * 60;
        int hh = mi * 60;
        int dd = hh * 24;

        long day = ms / dd;
        long hour = (ms - day * dd) / hh;
        long minute = (ms - day * dd - hour * hh) / mi;

        String strDay = "" + day; //天
        String strHour = "" + hour;//小时
        String strMinute = "" + minute;//分钟

        if (day > 0) {
            return strDay + "天" + strHour + "时" + strMinute + "分";
        } else {
            if (hour > 0) {
                return strHour + "时" + strMinute + "分";
            } else {
                return strMinute + "分";
            }
        }
    }

    /**
     * 毫秒转换时、分
      */
    public static String lengthTime(long ms) {
        int ss = 1000;
        int mi = ss * 60;
        int hh = mi * 60;

        long hour = ms / hh;
        long minute = (ms - hour * hh) / mi;

        String strHour = "" + hour;//小时
        String strMinute = "" + minute;//分钟
        if (hour > 0) {
            return strHour + "时" + strMinute + "分";
        } else {
            return strMinute + "分";
        }
    }


    /**
     * 毫秒转天
     */
    public static String toDay(long ms) {
        int ss = 1000;
        int mi = ss * 60;
        int hh = mi * 60;
        int dd = hh * 24;

        long day = ms / dd;
        String strDay = day < 10 ? "0" + day : "" + day; //天
        return strDay + "天";
    }

    /**
     * 将毫秒转换成小时、分钟
     *
     * @param ms
     * @return
     */
    public static String toHour(long ms) {
        int mi = 1000 * 60;
        int hh = mi * 60;

        long hour = ms / hh;
        long minute = (ms - hour * hh) / mi;

        //小时
        String strHour = hour < 10 ? "0" + hour : "" + hour;
        //分钟
        String strMinute = minute < 10 ? "0" + minute : "" + minute;
        return strHour + "时" + strMinute + "分钟 ";
    }

    /**
     * 将分钟转换成小时、分钟
     */
    public static String toHourByMinute(long ms) {
        int mi = 60;
        long hour = ms / mi;
        long minute = ms - hour * mi;
        if (hour < 1) {
            return minute + "分钟 ";
        }
        return hour + "时" + minute + "分钟 ";
    }

    /**
     * 时间戳转换成小时
     */
    public static String hour(long ms) {
        long hour = ms / 1000 / 60 / 60;
        return hour + "";
    }

    /**
     * 时间戳转化为Date
      */
    public static Date stampToTime(long ms) {
        int length = String.valueOf(ms).length();
        SimpleDateFormat format =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String d = format.format(length == 13 ? ms : ms*1000L);
        Date date = null;
        try {
            date = format.parse(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

}